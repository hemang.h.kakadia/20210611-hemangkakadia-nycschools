package com.example.nycschools.schools.service;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author Hemang Kakadia
 *
 * Generic interface to process the outcome of API call
 */
public interface APIResponseHandler {

    /**
     * Method to be call after Successful API call
     * @param response - JSONArray from API call
     */
    void onSuccess(JSONArray response);

    /**
     * Method to be call after failed API call
     * @param error - VolleyError Object from API call
     */
    void onError(VolleyError error);
}
