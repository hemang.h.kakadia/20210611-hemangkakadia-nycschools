package com.example.nycschools.schools.ui.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nycschools.schools.model.School;
import com.example.nycschools.R;
import java.util.ArrayList;

/**
 * @author Hemang Kakadia
 *
 * Recyclerview Adapter for the list view of Schools
 */
public class SchoolListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;

    /**
     * Arraylist of the data for the list adapter
     */
    ArrayList<School> schoolArrayList;

    /**
     * Click Listener holder passed from view
     */
    View.OnClickListener clickListener;

    public SchoolListAdapter(Context context, ArrayList<School> userArrayList, View.OnClickListener clickListener) {
        this.context = context;
        this.schoolArrayList = userArrayList;
        this.clickListener = clickListener;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(context).inflate(R.layout.school_list_view_item,parent,false);
        return new School_RVVH(rootView, clickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        School school = schoolArrayList.get(position);
        School_RVVH viewHolder= (School_RVVH) holder;

        viewHolder.itemLayout.setTag(position);
        viewHolder.txtView_name.setText(school.getSchoolName());
        viewHolder.txtView_city.setText(school.getCity());
    }

    @Override
    public int getItemCount() {
        return schoolArrayList.size();
    }

    /**
     * @author Hemang Kakadia
     * Viwe Holder class for School List
     */
    static class School_RVVH extends RecyclerView.ViewHolder {
        LinearLayout itemLayout;
        TextView txtView_name;
        TextView txtView_city;

        public School_RVVH(@NonNull View itemView, View.OnClickListener clickListener) {
            super(itemView);
            itemLayout = itemView.findViewById(R.id.llItemLayout);
            itemLayout.setOnClickListener(clickListener);
            txtView_name = itemView.findViewById(R.id.tvSchoolName);
            txtView_city = itemView.findViewById(R.id.tvCity);
        }
    }
}