package com.example.nycschools.schools.ui.list;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.VolleyError;
import com.example.nycschools.MainActivity;
import com.example.nycschools.R;
import com.example.nycschools.schools.model.School;
import com.example.nycschools.schools.service.APICalls;
import com.example.nycschools.schools.service.APIResponseHandler;
import com.example.nycschools.schools.ui.detail.SchoolDetailFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * @author Hemang Kakadia
 * Fragment class to show list of Schools
 */
public class SchoolListFragment extends Fragment implements APIResponseHandler, View.OnClickListener {

    private SchoolViewModel viewModel;
    private Context context;
    private RecyclerView recyclerView;
    private SchoolListAdapter recyclerViewAdapter;

    public static SchoolListFragment newInstance() {
        return new SchoolListFragment();
    }
    View.OnClickListener clickListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment, container, false);
        recyclerView = view.findViewById(R.id.rv_school_list);
        ((MainActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((MainActivity)getActivity()).updateScreenTitle("NYC School List");
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = this.getContext();
        clickListener = this;
        viewModel = new ViewModelProvider(requireActivity()).get(SchoolViewModel.class);
        viewModel.getSchoolListMutableLiveData().observe(getViewLifecycleOwner(), schoolListUpdateObserver);
        // TODO: Use the ViewModel
        getSchoolList();
    }

    /**
     * Observer to observe data change in school list
     */
    Observer<ArrayList<School>> schoolListUpdateObserver = new Observer<ArrayList<School>>() {
        @Override
        public void onChanged(ArrayList<School> schoolArrayList) {
            recyclerViewAdapter = new SchoolListAdapter(context,schoolArrayList, clickListener);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(recyclerViewAdapter);
        }
    };

    /**
     * To call API for School List
     */
    private void getSchoolList() {
        APICalls.getInstance(getContext()).makeCall("https://data.cityofnewyork.us/resource/s3k6-pzi2.json", this);
    }

    /**
     * Successful API call will be processed here
     * @param response - JSONArray from the API response
     */
    @Override
    public void onSuccess(JSONArray response) {
        ArrayList<School> schoolArrayList = new ArrayList<>();

        int intResponseLength = response.length();
        if(intResponseLength > 0){
            for(int countItem = 0;countItem<intResponseLength;countItem++){
                try{
                    JSONObject schoolJSON = response.getJSONObject(countItem);
                    School school = new School();
                    school.prepareObjectFromJSON(schoolJSON);
                    schoolArrayList.add(school);
                } catch (JSONException exception) {
                    // ToDO: Handle the exception scenario if required
                }
            }


        }
        if (viewModel!=null)
        viewModel.updateList(schoolArrayList);
    }

    /**
     * Failed API call will be handled here
     * @param error - Volley Error object from API error
     */
    @Override
    public void onError(VolleyError error) {
        new AlertDialog.Builder(getContext())
                .setMessage(error.getMessage())
                .setCancelable(true)
                .setNegativeButton("Close", null)
                .show();
    }

    /**
     * Will be called upon click of list item.
     * @param v - View object which is clicked
     */
    @Override
    public void onClick(View v) {
        int iPosition = Integer.parseInt(v.getTag().toString());
        School selectedSchool = viewModel.getSchoolList().get(iPosition);
        viewModel.updateSelectedSchool(selectedSchool);

        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, SchoolDetailFragment.newInstance())
                .addToBackStack("SchoolList")
                .commit();
    }
}