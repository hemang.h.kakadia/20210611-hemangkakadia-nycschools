package com.example.nycschools.schools.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Hemang Kakadia
 * POJO class for School Details
 */
public class SchoolDetail {
    /**
     * Holds School Name as String
     */
    private String school_name;
    /**
     * Holds DBN as Sting which is unique identifier
     */
    private String dbn;
    /**
     * Holds total number of Test Takers of the school
     */
    private String testTakers;
    /**
     * Holds Reading Average score of the School
     */
    private String readingAVG;
    /**
     * Holds Writing Average score of the School
     */
    private String writingAVG;
    /**
     * Holds Math Average score of the School
     */
    private String mathAVG;

    /**
     *
     * @return School Name as string
     */
    public String getSchoolName() {
        return school_name;
    }

    /**
     *
     * @param school_name as String
     */
    public void setSchoolName(String school_name) {
        this.school_name = school_name;
    }

    /**
     *
     * @return DBN - Unique identifier for the school
     */
    public String getDbn() {
        return dbn;
    }

    /**
     *
     * @param dbn - Unique identifier for the school
     */
    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    /**
     *
     * @return total Test Takers at school
     */
    public String getTestTakers() {
        return testTakers;
    }

    /**
     *
     * @param testTakers - Total Test Takers at School
     */
    public void setTestTakers(String testTakers) {
        this.testTakers = testTakers;
    }

    /**
     *
     * @return Reading Average scored by School
     */
    public String getReadingAVG() {
        return readingAVG;
    }

    /**
     *
     * @param readingAVG - Reading Average scored by School
     */
    public void setReadingAVG(String readingAVG) {
        this.readingAVG = readingAVG;
    }

    /**
     *
     * @return Writing Average scored by School
     */
    public String getWritingAVG() {
        return writingAVG;
    }

    /**
     *
     * @param writingAVG - Writing Average scored by School
     */
    public void setWritingAVG(String writingAVG) {
        this.writingAVG = writingAVG;
    }

    /**
     *
     * @return Math Average scored by School
     */
    public String getMathAVG() {
        return mathAVG;
    }

    /**
     *
     * @param mathAVG - Math Average scored by School
     */
    public void setMathAVG(String mathAVG) {
        this.mathAVG = mathAVG;
    }

    /**
     *
     * @param jsonObject - JSON Object of the school details
     */
    public void prepareObjectFromJSON(JSONObject jsonObject)
    {
        try {
            setSchoolName(jsonObject.getString("school_name"));
        } catch (JSONException exception) {
            setSchoolName("----");
        }
        try {
            setDbn(jsonObject.getString("dbn"));

        } catch (JSONException exception) {
            setDbn("----");
        }
        try {
            setTestTakers(jsonObject.getString("num_of_sat_test_takers"));
        } catch (JSONException exception) {
            setTestTakers("----");
        }
        try {
            setReadingAVG(jsonObject.getString("sat_critical_reading_avg_score"));
        } catch (JSONException exception) {
            setReadingAVG("----");
        }
        try {
            setWritingAVG(jsonObject.getString("sat_writing_avg_score"));
        } catch (JSONException exception) {
            setWritingAVG("----");
        }
        try {
            setMathAVG(jsonObject.getString("sat_math_avg_score"));
        } catch (JSONException exception) {
            setMathAVG("----");
        }
    }
}
