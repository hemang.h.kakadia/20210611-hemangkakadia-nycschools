package com.example.nycschools.schools.ui.list;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.nycschools.schools.model.School;

import java.util.ArrayList;

/**
 * @author Hemang Kakadia
 * ViewModel Class for School List View
 */
public class SchoolViewModel extends ViewModel {

    //Mutable live data for School list
    MutableLiveData<ArrayList<School>> schoolListLiveData;

    //Mutable live data for Selected School
    MutableLiveData<School> selectedSchool;

    public SchoolViewModel() {
        schoolListLiveData = new MutableLiveData<>();
        selectedSchool = new MutableLiveData<>();
        updateList(defaultList());
    }

    public MutableLiveData<ArrayList<School>> getSchoolListMutableLiveData() {
        return schoolListLiveData;
    }
    public MutableLiveData<School> getSelectedSchoolMutableLiveData() {
        return selectedSchool;
    }

    public ArrayList<School> getSchoolList() {
        return schoolListLiveData.getValue();
    }
    public School getSelectedSchool() {
        return selectedSchool.getValue();
    }

    /**
     * To update New set of value
     * @param schoolArrayList
     */
    public void updateList(ArrayList<School> schoolArrayList){
        schoolListLiveData.setValue(schoolArrayList);
    }

    /**
     * To update New set of value
     * @param selectedSchool
     */
    public void updateSelectedSchool(School selectedSchool){
        this.selectedSchool.setValue(selectedSchool);
    }

    /**
     * To generate default value set
     * @return
     */
    public ArrayList<School> defaultList(){
        ArrayList<School> schoolArrayList = new ArrayList<>();

        School school = new School();
        school.setSchoolName("Loading School Details");
        school.setCity("");

        schoolArrayList.add(school);
        return schoolArrayList;
    }
}