package com.example.nycschools.schools.ui.detail;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.example.nycschools.MainActivity;
import com.example.nycschools.R;
import com.example.nycschools.schools.model.School;
import com.example.nycschools.schools.model.SchoolDetail;
import com.example.nycschools.schools.service.APICalls;
import com.example.nycschools.schools.service.APIResponseHandler;
import com.example.nycschools.schools.ui.list.SchoolViewModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * @author Hemang Kakadia
 * Fragment class to show School Details of the school selected from School List
 */
public class SchoolDetailFragment extends Fragment implements APIResponseHandler {

    /**
     * View model object of School Detail
     */
    private SchoolDetailViewModel detailsViewModel;
    /**
     * View model object of School
     */
    private SchoolViewModel schoolViewModel;
    /**
     * View object of Fragment root container
     */
    private View fragmentView;

    public static SchoolDetailFragment newInstance() {
        return new SchoolDetailFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.school_detail_fragment, container, false);
        ((MainActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainActivity)getActivity()).updateScreenTitle("School Details");

        return fragmentView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        detailsViewModel = new ViewModelProvider(requireActivity()).get(SchoolDetailViewModel.class);
        detailsViewModel.getSchoolDetailsMutableLiveData().observe(getViewLifecycleOwner(), schoolDetailsUpdateObserver);

        schoolViewModel = new ViewModelProvider(requireActivity()).get(SchoolViewModel.class);
        schoolViewModel.getSelectedSchoolMutableLiveData().observe(getViewLifecycleOwner(), schoolListUpdateObserver);

        getSchoolDetails();
    }

    /**
     * Observer to observe data change in selected school
     */
    Observer<School> schoolListUpdateObserver = new Observer<School>() {
        @Override
        public void onChanged(School selectedSchool) {
            fragmentView.setTag(selectedSchool.getDbn());
            TextView tvSchoolName = fragmentView.findViewById(R.id.tvName);
            TextView tvSchoolCity = fragmentView.findViewById(R.id.tvCity);

            tvSchoolName.setText(selectedSchool.getSchoolName());
            tvSchoolCity.setText(selectedSchool.getCity());
        }
    };

    /**
     * Observer to observe data change in School Details
     */
    Observer<ArrayList<SchoolDetail>> schoolDetailsUpdateObserver = new Observer<ArrayList<SchoolDetail>>() {
        @Override
        public void onChanged(ArrayList<SchoolDetail> schoolArrayList) {

            int intTotalSchools = schoolArrayList.size();
            School selectedSchool = schoolViewModel.getSelectedSchool();
            String selectedDBN = selectedSchool.getDbn();

            for (int i=0; i<intTotalSchools; i++)
            {
                SchoolDetail schoolDetails = schoolArrayList.get(i);
                if (schoolDetails.getDbn().equalsIgnoreCase(selectedDBN)) {
                    TextView tvReading = fragmentView.findViewById(R.id.tvReading);
                    TextView tvWriting = fragmentView.findViewById(R.id.tvWriting);
                    TextView tvMath = fragmentView.findViewById(R.id.tvMath);
                    TextView tvSatTest = fragmentView.findViewById(R.id.tvSatTest);

                    tvReading.setText(schoolDetails.getReadingAVG());
                    tvWriting.setText(schoolDetails.getWritingAVG());
                    tvMath.setText(schoolDetails.getMathAVG());
                    tvSatTest.setText(schoolDetails.getTestTakers());
                }

            }
        }
    };

    /**
     * To call API for School Details
     */
    private void getSchoolDetails() {
        APICalls.getInstance(getContext()).makeCall("https://data.cityofnewyork.us/resource/f9bf-2cp4.json", this);
    }

    /**
     * Successful API call will be processed here
     * @param response - JSONArray of API response
     */
    @Override
    public void onSuccess(JSONArray response) {
        ArrayList<SchoolDetail> schoolDetailsArrayList = new ArrayList<>();

        int intResponseLength = response.length();
        if(intResponseLength > 0){
            for(int countItem = 0;countItem<intResponseLength;countItem++){
                try{
                    JSONObject schoolJSON = response.getJSONObject(countItem);
                    SchoolDetail school = new SchoolDetail();
                    school.prepareObjectFromJSON(schoolJSON);
                    schoolDetailsArrayList.add(school);
                } catch (JSONException exception) {
                    // ToDO: Handle the exception scenario if required
                }
            }

        }
        detailsViewModel.updateSchoolDetails(schoolDetailsArrayList);
    }

    /**
     * Failed API call will be handled here
     * @param error - Volley Error object from API error
     */
    @Override
    public void onError(VolleyError error) {
        new AlertDialog.Builder(getContext())
                .setMessage(error.getMessage())
                .setCancelable(true)
                .setNegativeButton("Close", null)
                .show();
    }

}