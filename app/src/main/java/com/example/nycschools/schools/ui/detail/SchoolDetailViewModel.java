package com.example.nycschools.schools.ui.detail;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.nycschools.schools.model.School;
import com.example.nycschools.schools.model.SchoolDetail;

import java.util.ArrayList;

/**
 * @author Hemang Kakadia
 * ViewModel Class for School Detail View
 */
public class SchoolDetailViewModel extends ViewModel {
    /**
     * Mutable live data object to hold School Detail
     */
    MutableLiveData<ArrayList<SchoolDetail>> schoolDetailsLiveData;

    public SchoolDetailViewModel() {
        schoolDetailsLiveData = new MutableLiveData<>();
    }

    public MutableLiveData<ArrayList<SchoolDetail>> getSchoolDetailsMutableLiveData() {
        return schoolDetailsLiveData;
    }

    /**
     * To retrieve the Arraylist of the data
     * @return
     */
    public ArrayList<SchoolDetail> getSchoolDetailList() {
        return schoolDetailsLiveData.getValue();
    }

    /**
     * To update New set of value
     * @param schoolDetailArrayList
     */
    public void updateSchoolDetails(ArrayList<SchoolDetail> schoolDetailArrayList){
        schoolDetailsLiveData.setValue(schoolDetailArrayList);
    }

    /**
     * To generate default value set
     * @return
     */
    public ArrayList<SchoolDetail> defaultList(){
        ArrayList<SchoolDetail> schoolDetailArrayList = new ArrayList<>();

        SchoolDetail schoolDetail = new SchoolDetail();
        schoolDetail.setSchoolName("Loading School Details");

        schoolDetailArrayList.add(schoolDetail);
        return schoolDetailArrayList;
    }
}