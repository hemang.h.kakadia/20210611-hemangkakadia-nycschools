package com.example.nycschools.schools.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Hemang Kakadia
 * POJO class for School
 */
public class School {
    /**
     * Holds School Name as String
     */
    private String school_name;
    /**
     * Holds City of the School
     */
    private String city;
    /**
     * Holds DBN as Sting which is unique identifier
     */
    private String dbn;

    public String getSchoolName() {
        return school_name;
    }
    public void setSchoolName(String school_name) {
        this.school_name = school_name;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    public void prepareObjectFromJSON(JSONObject jsonObject)
    {
        try {
            setSchoolName(jsonObject.getString("school_name"));
        } catch (JSONException exception) {
            setSchoolName("----");
        }
        try {
            setCity(jsonObject.getString("city"));
        } catch (JSONException exception) {
            setCity("----");
        }
        try {
            setDbn(jsonObject.getString("dbn"));
        } catch (JSONException exception) {
            setDbn("----");
        }
    }
}
