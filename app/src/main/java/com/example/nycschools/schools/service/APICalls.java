package com.example.nycschools.schools.service;

import android.app.AlertDialog;
import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;

/**
 * @author Hemang Kakadia
 * Singleton class to make API calls.
 */
public class APICalls {
    /**
     * Holds Singleton class object
     */
    static APICalls apiCall = null;
    /**
     * The queue for the API call requests
     */
    RequestQueue callQueue =null;
    /**
     * Context to be used for API calls so they stay relevant.
     */
    Context context = null;

    /**
     * Private constructor to achieve Singleton nature
     * @param context
     */
    private APICalls (Context context)
    {
        this.context = context;
        if (callQueue ==null)
            callQueue = Volley.newRequestQueue(context);
    }

    /**
     * To retrieve Singleton object of the class
     * @param context
     * @return
     */
    public static APICalls getInstance(Context context)
    {
        if (apiCall==null)
            apiCall = new APICalls(context);
        return apiCall;
    }

    /**
     * Method to invoke API Calls
     * @param url
     * @param responseHandler
     */
    public void makeCall(String url, APIResponseHandler responseHandler){
        if (url==null || url.length()==0)
            return;

        AlertDialog loadingDialog = new AlertDialog.Builder(context)
                .setMessage("Loading...")
                .setCancelable(false)
                .show();

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
                responseHandler.onSuccess(response);
                loadingDialog.dismiss();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Handle error
                responseHandler.onError(error);
                loadingDialog.dismiss();
            }
        });

        callQueue.add(jsonArrayRequest);
    }
}
