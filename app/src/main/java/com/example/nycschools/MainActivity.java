package com.example.nycschools;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.nycschools.schools.ui.detail.SchoolDetailViewModel;
import com.example.nycschools.schools.ui.list.SchoolListFragment;
import com.example.nycschools.schools.ui.list.SchoolViewModel;

/**
 * @author Hemang Kakadia
 *
 * Main Activity Class
 */
public class MainActivity extends AppCompatActivity {

    //View for the Screen Title
    TextView tvScreenTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, SchoolListFragment.newInstance())
                    .commitNow();
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.tool_title);
        tvScreenTitle = (TextView) findViewById(R.id.tvTitle);
        final SchoolViewModel schoolViewModel = new ViewModelProvider(this).get(SchoolViewModel.class);
        final SchoolDetailViewModel schoolDetailViewModel = new ViewModelProvider(this).get(SchoolDetailViewModel.class);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                ((MainActivity)this).onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 1) {
            fm.popBackStackImmediate();
        }
    }

    /**
     * Screen Title will be update with passed value.
     * @param strTitle
     */
    public void updateScreenTitle(String strTitle) {
        if (tvScreenTitle != null)
            tvScreenTitle.setText(strTitle);
    }
}