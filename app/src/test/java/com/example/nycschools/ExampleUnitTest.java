package com.example.nycschools;

import com.example.nycschools.schools.model.School;
import com.example.nycschools.schools.model.SchoolDetail;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(MockitoJUnitRunner.class)
public class ExampleUnitTest {
    @Test
    public void jsonToSchoolObject() throws JSONException {
        //JSONObject wrongResponse = new JSONObject("\"dn\":\"02M260\", \"school_nme\":\"Clinton School Writers & Artists, M.S. 260\",\"ciy\":\"M\"");
        School school = new School();

        JSONObject dummyResponse = new JSONObject();
        try {
            dummyResponse.put("dbn", "02M260");
            dummyResponse.put("school_name", "Dummy Name");
            dummyResponse.put("city", "Dummy City");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        school.prepareObjectFromJSON(dummyResponse);
        assertEquals("Valid City Failed!", school.getCity(), "Dummy City");
        assertEquals("Valid DBN Failed!", school.getDbn(), "02M260");
        assertEquals("Valid School Name Failed!", school.getSchoolName(), "Dummy Name");

        JSONObject wrongResponse = new JSONObject();
        try {
            wrongResponse.put("dn", "02M260");
            wrongResponse.put("scol_name", "Dummy Name");
            wrongResponse.put("cy", "Dummy City");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        school.prepareObjectFromJSON(wrongResponse);
        assertEquals("Invalid City Failed!", school.getCity(), "----");
        assertEquals("Invalid City Failed!", school.getDbn(), "----");
        assertEquals("Invalid City Failed!", school.getSchoolName(), "----");
    }
    @Test
    public void jsonToSchoolDetailObject() throws JSONException {
        //JSONObject wrongResponse = new JSONObject("\"dn\":\"02M260\", \"school_nme\":\"Clinton School Writers & Artists, M.S. 260\",\"ciy\":\"M\"");
        SchoolDetail school = new SchoolDetail();

        JSONObject dummyResponse = new JSONObject();
        try {
            dummyResponse.put("dbn", "02M260");
            dummyResponse.put("school_name", "Dummy Name");
            dummyResponse.put("num_of_sat_test_takers", "500");
            dummyResponse.put("sat_critical_reading_avg_score", "500");
            dummyResponse.put("sat_writing_avg_score", "500");
            dummyResponse.put("sat_math_avg_score", "500");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        school.prepareObjectFromJSON(dummyResponse);
        assertEquals("Valid Math AVG Failed!", school.getMathAVG(), "500");
        assertEquals("Valid Reading AVG Failed!", school.getReadingAVG(), "500");
        assertEquals("Valid Test Takers Failed!", school.getTestTakers(), "500");
        assertEquals("Valid Writing AVG Failed!", school.getWritingAVG(), "500");
        assertEquals("Valid DBN Failed!", school.getDbn(), "02M260");
        assertEquals("Valid School Name Failed!", school.getSchoolName(), "Dummy Name");

        JSONObject wrongResponse = new JSONObject();
        try {
            wrongResponse.put("dn", "02M260");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        school.prepareObjectFromJSON(wrongResponse);
        assertEquals("Invalid Math AVG Failed!", school.getMathAVG(), "----");
        assertEquals("Invalid Reading AVG Failed!", school.getReadingAVG(), "----");
        assertEquals("Invalid Test Takers Failed!", school.getTestTakers(), "----");
        assertEquals("Invalid Writing AVG Failed!", school.getWritingAVG(), "----");
        assertEquals("Invalid DBN Failed!", school.getDbn(), "----");
        assertEquals("Invalid School Name Failed!", school.getSchoolName(), "----");
    }
}